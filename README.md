# Run Local

```
npm run start
```

Le port par défaut est 3000

## Configurer le mock

Les variables d'environnement suivantes peuvent être customisées au travers d'un fichier .env ou des varaibles d'environnement du container:

PORT default to 3000
STEPBACK_BASE_PATH default to http://localhost:18080
STEPFRONT_BASE_PATH  default to http://localhost:4200
STEPFRONT_API_PATH  default to http://localhost:4200/local
MOCK_BASE_PATH default to http://localhost:13000

Un fichier .env.sample est fourni.
Le .env est volontairement exclu de git.


## Configurer l'env local pour pointer sur le mock:


WS_SELLIGENT_LOGIN_ENDPOINT=http://localhost:3000/selligent/SessionServices.asmx
WS_SELLIGENT_SCRIPT_ENDPOINT=http://localhost:3000/selligent/ScriptServices.asmx
WS_TARIFICATEUR_SANTEINDIV_ENDPOINT=http://localhost:3000/tarificateur-santeindiv
WS_TARIFICATEUR_DOCUMENTS_ENDPOINT=http://localhost:3000/tarificateur-documents
WS_EDIGROUP_ENDPOINT=http://localhost:3000/edigroup
WS_ESIGN_SIGNATURE_ENVELOPPES_ENDPOINT=http://localhost:3000/esign
WS_NUMEROTEUR_CONTRAT_ENDPOINT=http://localhost:3000/numeroteur
WS_JOACHIM_ENDPOINT=http://localhost:3000/joachim/Portail/SP_JoachimExternal

# Run Docker

## Configurer l'env local pour pointer sur le mock sous docker:

Un nouveau docker-compose.yml est dispo dans ce repo pour ne demarrer que le backend step et les mocks.
Cela permet de soulager un peu la RAM et CPU en local...

Vous devriez le copier dans le dossier docker-stepbackend pour l'utiliser
Ce projet doit être checkout dans un dossier pvp_1b_mockserver au même niveau que docker-stepbackend 


Pour utiliser le mock docker changer la conf IDEA pour utiliser les url suivantes:

WS_SELLIGENT_LOGIN_ENDPOINT=http://localhost:13000/selligent/SessionServices.asmx
WS_SELLIGENT_SCRIPT_ENDPOINT=http://localhost:13000/selligent/ScriptServices.asmx
WS_TARIFICATEUR_SANTEINDIV_ENDPOINT=http://localhost:13000/tarificateur-santeindiv
WS_TARIFICATEUR_DOCUMENTS_ENDPOINT=http://localhost:13000/tarificateur-documents
WS_EDIGROUP_ENDPOINT=http://localhost:13000/edigroup
WS_ESIGN_SIGNATURE_ENVELOPPES_ENDPOINT=http://localhost:13000/esign
WS_NUMEROTEUR_CONTRAT_ENDPOINT=http://localhost:13000/numeroteur
WS_JOACHIM_ENDPOINT=http://localhost:13000/joachim/Portail/SP_JoachimExternal
