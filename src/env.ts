export const PORT = process.env.PORT || 13000;
export const STEPBACK_BASE_PATH = process.env.STEPBACK_BASE_PATH || "http://localhost:28080";
export const STEPFRONT_BASE_PATH = process.env.STEPFRONT_BASE_PATH || "http://localhost:4200";
export const STEPFRONT_API_PATH = process.env.STEPFRONT_API_PATH || `${STEPFRONT_BASE_PATH}/api`;
export const MOCK_BASE_PATH = process.env.MOCK_BASE_PATH || "http://localhost:13000";
