require('dotenv').config()

import Koa from "koa";
import Router from "koa-router";

import logger from "koa-logger";
import json from "koa-json";
import bodyParser from "koa-bodyparser";

import { PORT } from "./env";

import { installDocumentRoutes } from "./routes/documents.routes";
import { installEdigroupRoutes } from "./routes/edigroup.routes";
import { installESignRoutes } from "./routes/esign.routes";
import { installNumeroteurRoutes } from "./routes/numeroteur.routes";
import { installSelligentRoutes } from "./routes/selligent.routes";
import { installTarificateurRoutes } from "./routes/tarificateur.routes";
import { installJoachimRoutes } from "./routes/joachim.routes";

const app = new Koa();
const router = new Router();


// Middlewares
app.use(json());
app.use(logger());
app.use(bodyParser({
  enableTypes: ['json', 'xml', 'text'],
  jsonLimit: '100mb',
  textLimit: '100mb',
  formLimit: '100mb',
  xmlLimit: '100mb',
} as any));

installTarificateurRoutes(router);
installDocumentRoutes(router);
installEdigroupRoutes(router);
installESignRoutes(router);
installNumeroteurRoutes(router);
installSelligentRoutes(router);
installJoachimRoutes(router);

// Routes
app.use(router.routes()).use(router.allowedMethods());

app.listen(PORT, () => {
  console.log("Koa started");
});