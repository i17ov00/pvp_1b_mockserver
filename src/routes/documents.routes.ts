import Router from "koa-router";
import { readFileSync } from "fs";

export function installDocumentRoutes(router: Router) {

    router.get("/tarificateur-documents/documents/:codeDocument", async (ctx, next) => {
        ctx.status = 200;
        ctx.set('Content-Type', 'application/json');
        const enveloppesResponse = readFileSync("./mocks/documents/onedoc.response.json", "utf-8")
        ctx.body = enveloppesResponse;
        await next();
    });
}