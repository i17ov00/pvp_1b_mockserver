import Router from "koa-router";
import { readFileSync } from "fs";

export function installEdigroupRoutes(router: Router) {

    router.post("/edigroup/documents/templateMono", async (ctx, next) => {
        ctx.status = 200;
        ctx.set('Content-Type', 'application/xml');
        const templateMonoResponse = readFileSync("./mocks/edigroup/templateMono.response.xml", "utf-8")
        ctx.body = templateMonoResponse;
        await next();
    });

    router.post("/edigroup/documents", async (ctx, next) => {
        ctx.status = 200;
        ctx.set('Content-Type', 'application/xml');
        const documentsResponse = readFileSync("./mocks/edigroup/documents.response.xml", "utf-8")
        ctx.body = documentsResponse;
        await next();
    });
}