import Router from "koa-router";
import { readFileSync } from "fs";
import { createUUID } from "../helpers"

import { STEPFRONT_API_PATH, MOCK_BASE_PATH } from "../env";

export function installESignRoutes(router: Router) {
    
    router.get("/esign", async (ctx, next) => {
        ctx.status = 200;
        ctx.set('Content-Type', 'text/html');
        let ecranSignatureResponse = readFileSync("./mocks/esign/ui.html", "utf-8");
        ecranSignatureResponse = ecranSignatureResponse.replace(/##RETURN_URL##/g, ctx.query['returnURL'])
        ctx.body = ecranSignatureResponse;
        await next();
    });

    router.post("/esign/enveloppes", async (ctx, next) => {
        ctx.status = 200;
        ctx.request.socket.setTimeout(20 * 1000);
        ctx.set('Content-Type', 'application/json');
        let enveloppesResponse = readFileSync("./mocks/esign/enveloppes.response.json", "utf-8")
        enveloppesResponse = enveloppesResponse.replace(/##RANDOM_UUID##/g, () => createUUID())
        ctx.body = enveloppesResponse;
        await next();
    });

    router.get("/esign/enveloppes/:idEnveloppe/signataires/:idSignataire/ecranSignature", async (ctx, next) => {
        ctx.status = 200;
        ctx.set('Content-Type', 'application/json');

        let returnURL = "##STEPFRONT_API_PATH##/souscription/signature/##ID_ENVELOPPE##/retour-signature-contrat"
        returnURL = returnURL.replace(/##STEPFRONT_API_PATH##/g, STEPFRONT_API_PATH)
        returnURL = returnURL.replace(/##ID_ENVELOPPE##/g, ctx.params['idEnveloppe'])

        let ecranSignatureResponse = readFileSync("./mocks/esign/ecranSignature.response.json", "utf-8");
        ecranSignatureResponse = ecranSignatureResponse.replace(/##MOCK_BASE_PATH##/g, MOCK_BASE_PATH)
        ecranSignatureResponse = ecranSignatureResponse.replace(/##ID_ENVELOPPE##/g, ctx.params['idEnveloppe'])
        ecranSignatureResponse = ecranSignatureResponse.replace(/##RETURN_URL##/g, returnURL)
        ctx.body = ecranSignatureResponse;
        await next();
    });

    router.get("/esign/enveloppes/:idEnveloppe/documents/:idDocument", async (ctx, next) => {
        ctx.status = 200;
        ctx.set('Content-Type', 'application/pdf');
        const ecranSignatureResponse = readFileSync("./mocks/esign/document.response.pdf")
        ctx.body = ecranSignatureResponse;
        await next();
    });

}