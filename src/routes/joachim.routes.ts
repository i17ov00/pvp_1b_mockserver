import Router from "koa-router";
import { readFileSync, writeFileSync } from "fs";

let i = 1;

export function installJoachimRoutes(router: Router) {

    router.get("/joachim/Portail/SP_JoachimExternal", async (ctx, next) => {
        ctx.status = 200;
        ctx.set('Content-Type', 'text/xml');
        const wsdl = readFileSync("./mocks/joachim/joachimexternal.wsdl.xml", 'utf-8')
        ctx.body = wsdl;
        await next();
    });

    router.post("/joachim/Portail/SP_JoachimExternal", async (ctx, next) => {
        ctx.status = 200;
        // writeFileSync('./logreq-joachim' + i + '.log', ctx.request.rawBody, 'utf-8');
        // i++;
        ctx.set('Content-Type', 'text/xml');
        if (ctx.request.headers['soapaction'].endsWith("/ReceptionFormulaireInternet\"")) {
            const resBody: string = readFileSync("./mocks/joachim/ReceptionFormulaireInternetResponse.xml", 'utf-8');
            ctx.body = resBody;
        }
        await next();
    });


}