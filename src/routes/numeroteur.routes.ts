import Router from "koa-router";
import { readFileSync } from "fs";
import { createUUID, createRandomNumbers } from "../helpers"

export function installNumeroteurRoutes(router: Router) {

    router.post("/numeroteur/contrats/numeros", async (ctx, next) => {
        ctx.status = 200;
        ctx.set('Content-Type', 'application/json');
        let numerosResponse = readFileSync("./mocks/numeroteur/numeros.response.json", "utf-8");
        numerosResponse = numerosResponse.replace(/##RANDOM_UUID##/g, () => createUUID());
        numerosResponse = numerosResponse.replace(/##RANDOM_NUMBERS##/g, () => createRandomNumbers(11));
        ctx.body = numerosResponse;
        await next();
    });


    router.post("/numeroteur/contrats/statut", async (ctx, next) => {
        ctx.status = 200;
        ctx.set('Content-Type', 'application/json');
        let statusResponse = readFileSync("./mocks/numeroteur/status.response.json", "utf-8");
        ctx.body = statusResponse;
        await next();
    });
}