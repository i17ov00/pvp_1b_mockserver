import Router from "koa-router";
import { readFileSync } from "fs";

import { STEPBACK_BASE_PATH } from "../env";

function escapeXML(xml): string {
    return xml.replace(/&/g, '&amp;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;');
}

export function installSelligentRoutes(router: Router) {

    router.get("/selligent", async (ctx, next) => {
        ctx.status = 200;
        ctx.set('Content-Type', 'text/html');
        let ecranSelligent = readFileSync("./mocks/selligent/ui.html", "utf-8");
        ecranSelligent = ecranSelligent.replace(/##STEPBACK_BASE_PATH##/g, STEPBACK_BASE_PATH)
        ctx.body = ecranSelligent;
        await next();
    });

    router.get("/selligent/SessionServices.asmx", async (ctx, next) => {
        ctx.status = 200;
        ctx.set('Content-Type', 'text/xml');
        const wsdl = readFileSync("./mocks/selligent/SessionServices.wsdl.xml", 'utf-8')
        ctx.body = wsdl;
        await next();
    })

    router.post("/selligent/SessionServices.asmx", async (ctx, next) => {
        ctx.status = 200;
        ctx.set('Content-Type', 'text/xml');
        if (ctx.request.headers['soapaction'].endsWith("/Login\"")) {
            ctx.body = readFileSync("./mocks/selligent/getSession_result.xml", 'utf-8');
        }
        await next();
    });


    router.get("/selligent/ScriptServices.asmx", async (ctx, next) => {
        ctx.status = 200;
        ctx.set('Content-Type', 'text/xml');
        const wsdl = readFileSync("./mocks/selligent/ScriptServices.wsdl.xml", 'utf-8')
        ctx.body = wsdl;
        await next();
    })

    router.post("/selligent/ScriptServices.asmx", async (ctx, next) => {
        ctx.status = 200;
        ctx.set('Content-Type', 'text/xml');
        if (ctx.request.headers['soapaction'].endsWith("/ExecuteServerScript\"")) {
            const resBody: string = readFileSync("./mocks/selligent/ExecuteServerScriptResponse.xml", 'utf-8');
            let data: string = "";
            //get personne
            if (ctx.request.rawBody.includes('scriptNrid>28688398490058</')) {
                let mock = "./mocks/selligent/getPersonne_result.xml";
                if (ctx.request.rawBody.includes('paramsValue>50807494;160516433;')) {
                    mock = "./mocks/selligent/getPersonne_result_160516433.xml";
                } else if (ctx.request.rawBody.includes('paramsValue>29425808;222740785;')) {
                    mock = "./mocks/selligent/getPersonne_result_222740785.xml";
                } else if (ctx.request.rawBody.includes('paramsValue>25792830;639416854;')) {
                    mock = "./mocks/selligent/getPersonne_result_639416854.xml";
                } else if (ctx.request.rawBody.includes('paramsValue>65318261;913530566;')) {
                    mock = "./mocks/selligent/getPersonne_result_913530566.xml";
                } else if (ctx.request.rawBody.includes('paramsValue>88336641;859050338;')) {
                    mock = "./mocks/selligent/getPersonne_result_859050338.xml";
                }

                data = readFileSync(mock, 'utf-8');
            }
            //create personne
            if (ctx.request.rawBody.includes('scriptNrid>37199177350940</')) {
                data = readFileSync("./mocks/selligent/createPersonne_result.xml", 'utf-8');
            }
            // update personne
            if (ctx.request.rawBody.includes('scriptNrid>28458993569340</')) {
                data = readFileSync("./mocks/selligent/updatePersonne_result.xml", 'utf-8');
            }
            ctx.body = resBody.replace("##RESPONSE_CONTENT##", escapeXML(data));
        }
        await next();
    });
}