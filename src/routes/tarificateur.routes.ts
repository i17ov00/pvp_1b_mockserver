import Router from "koa-router";
import { readFileSync } from "fs";

export function installTarificateurRoutes(router: Router) {

    router.get("/tarificateur-santeindiv/produits/psp/formules-socles", async (ctx, next) => {
        ctx.status = 200;
        ctx.set('Content-Type', 'application/json');
        const responseContent = readFileSync("./mocks/tarificateur/formules-socles.response.json", "utf-8")
        ctx.body = responseContent;
        await next();
    });

    router.get("/tarificateur-santeindiv/produits/psp/formules-socles/:socle/formules", async (ctx, next) => {
        ctx.status = 200;
        ctx.set('Content-Type', 'application/json');
        const responseContent = readFileSync(`./mocks/tarificateur/formules-by-socles.${ctx.params['socle']}.response.json`, "utf-8")
        ctx.body = responseContent;
        await next();
    });

    router.post("/tarificateur-santeindiv/produits/psp/formules-recommandees", async (ctx, next) => {
        ctx.status = 200;
        ctx.set('Content-Type', 'application/json');
        const responseContent = readFileSync("./mocks/tarificateur/formules-recommandees.response.json", "utf-8")
        ctx.body = responseContent;
        await next();
    });

    router.get("/tarificateur-santeindiv/produits/psp/formules/:formule/garanties", async (ctx, next) => {
        ctx.status = 200;
        ctx.set('Content-Type', 'application/json');
        let responseContent;
        if (ctx.params.formule === "BIEN_ETRE") {
            responseContent = readFileSync("./mocks/tarificateur/garanties.BIEN_ETRE.response.json", "utf-8")
        }
        else if (ctx.params.formule === "CONFORT") {
            responseContent = readFileSync("./mocks/tarificateur/garanties.CONFORT.response.json", "utf-8")
        }
        else {
            responseContent = readFileSync("./mocks/tarificateur/garanties.FORMULE.response.json", "utf-8")
        }
        ctx.body = responseContent;
        await next();
    });

    router.post("/tarificateur-santeindiv/produits/psp/formules/:formule/tarification", async (ctx, next) => {
        ctx.status = 200;
        ctx.set('Content-Type', 'application/json');
        const responseContent = readFileSync("./mocks/tarificateur/tarification.response.json", "utf-8")
        ctx.body = responseContent;
        await next();
    });
}